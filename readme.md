# RPV SUSY Study 
## chang whan jung, korea university
### How to Use

You should locate your skim files folder in the main folder, named as **"skim rpvfit (year)/"**

You can make the histograms in the stdy folders.

### Trigger Efficiency Study

by using root, activate /stdy/TE/generator.C in the TE folder.

You can check your results at the /Plots/(year)
