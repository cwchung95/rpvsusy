#include<iostream>
using namespace std;

vector<float> frame(TString var = "nb"){
  vector<float> ret;
  if(var == "nb"){
    ret.push_back(-0.5);
    ret.push_back(10.5);
    ret.push_back(11);
  }
  else if(var == "njets"){
    ret.push_back(-0.5);
    ret.push_back(15.5);
    ret.push_back(16);
  }
  else if(var == "mj"){
    ret.push_back(0);
    ret.push_back(2000);
    ret.push_back(20);
  }
  else if(var == "ht"){
    ret.push_back(0);
    ret.push_back(2000);
    ret.push_back(20);
  }
  return ret;
}
