#include<stdio.h>
#include<iostream>
#include"./PlotDrawer.h"
#include"./histframe.h"
#include"./Path.h"
using namespace std;

void trig_eff(TString year = "2016", TString var = "nb", TString hconoff = "hton" ){
  
  /*Data Chain*/
  TChain cha("tree");
  TString path = rpvDataPath(year);
  cha.Add(path+"*SingleMuonRun*.root");
  
  /*frame make*/
  vector<float> fr = frame(var);
  TH1D *h1_den = new TH1D("h1_den","h1_den",fr.at(2),fr.at(0),fr.at(1));
  TH1D *h1_num = new TH1D("h1_num","h1_num",fr.at(2),fr.at(0),fr.at(1));
  TCanvas *canv = new TCanvas("canv","canv",800,800);

  /*denominator & numerator Drawing*/
  h1_den = Drawhist(fr,&cha,year,var,"den", hconoff);
  h1_num = Drawhist(fr,&cha,year,var,"num", hconoff);

  /*Drawing merged plot*/
  canv = MergedFrame(h1_den, h1_num, fr, var, year, hconoff);
}
