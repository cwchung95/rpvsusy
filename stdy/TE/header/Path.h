#include<iostream>
using namespace std;

TString rpvDataPath(TString year = "2016"){
  TString ret;
  if(year == "2016"){
    ret = "../../skim_trig_2016/";
  }
  else if(year == "2017"){
    ret = "../../skim_trig_2017/";
  }
  else if(year == "2018"){
    ret = "../../skim_trig_2018/";
  }
  return ret;
}
