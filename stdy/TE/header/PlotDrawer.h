#include<iostream>
using namespace std;

TH1D *Drawhist(vector<float> fr, TChain *ch, TString year = "2016", TString var="njets", TString nd = "den", TString hconoff="hton"){
  TString htcut;
  TH1D *h = new TH1D("h","h",fr.at(2), fr.at(0), fr.at(1));
  TString cutmj, cutnb, cutnjets;
  cutmj = "mj12>500";
  cutnb = "nbm>=1";
  cutnjets = "njets>=4";
  TString cut, trig;
  if(nd == "den"){
    trig = "(trig_isomu24 || trig_isomu27)";
  }
  else if(nd == "num"){
    trig = "(trig_isomu24 || trig_isomu27)";
    if(year == "2016"){
      trig = trig + "&& (trig_jet450 || trig_ht900)";
    }
    else{
      trig = trig + "&& trig_ht1050";
    }
  }
  if(var == "njets"){
    cut = cutnb+"&&"+cutmj;
    var = "njets";
    htcut = "&& ht > 1300";
  }
  else if(var == "nb"){
    cut = cutmj+"&&"+cutnjets;
    var = "nbm";
    htcut = "&& ht > 1300";
  }
  else if(var == "mj"){
    cut = cutnjets+"&&"+cutnb;
    var = "mj12";
    htcut = "&& ht > 1300";
  }
  else if(var == "ht"){
    cut = "1";
    var = "ht";
    htcut = " && 1 ";
  }
  if(hconoff=="hton"){
    cut = cut + htcut;
  }
  else if(hconoff=="htoff"){
  }
  ch->Draw(Form("min(%s,%f)>>h",var.Data(),fr.at(1)-0.00001),"weight*("+trig+"&&"+cut+")","goff");
  return h;
}

TCanvas *MergedFrame(TH1D *h1_den, TH1D *h1_num, vector<float> fr, TString var = "nb", TString year = "2016", TString hconoff = "hton"){
  TCanvas *c = new TCanvas("c","c",800,800);
  float begin = fr.at(0);
  float end = fr.at(1);
  int nbins = (int) fr.at(2);
  TH1D *h1_eff = dynamic_cast<TH1D*>(h1_num->Clone("h1_eff"));
  Double_t effmax = h1_eff->GetMaximum();

  Double_t max = h1_num->GetMaximum();
  Double_t min = h1_num->GetMinimum();

  c->DrawFrame(begin,0,end,1.5);
  
  h1_eff->SetTitle("");
  h1_eff->Divide(h1_num, h1_den, 1, 1, "B");
  h1_eff->SetLineColor(kBlue);
  h1_eff->SetMarkerColor(kBlue);
  h1_eff->SetMaximum(1.5);
  h1_eff->SetMinimum(0);

  h1_eff->GetYaxis()->SetTitle("Efficiency");
  h1_eff->GetXaxis()->SetTitle(var);
  h1_eff->SetTitleSize(0.04);
  h1_eff->GetXaxis()->SetLabelSize(0.02);
  h1_eff->GetYaxis()->SetLabelSize(0.02);

  h1_eff->SetMarkerStyle(20);
  h1_eff->SetMarkerSize(0.5);
  h1_eff->SetStats(0);
  h1_eff->SetLineStyle(1);

  h1_eff->Draw("");

  TGaxis *ax1 = new TGaxis(gPad->GetUxmax(),0,gPad->GetUxmax(),1.5,0,max*3,10510,"+L");
  
  ax1->SetLabelSize(0.02);
  ax1->SetLabelFont(40);
  
  ax1->SetTitle("Events");
  ax1->SetTitleSize(0.04);
  ax1->SetTitleOffset(1.3);
  ax1->SetTitleFont(40);

  ax1->Draw("same");

  //

  h1_den->SetLineColor(kBlack);
  h1_den->SetStats(0);
  h1_den->SetTitle("");
  h1_den->SetLineStyle(2);
  h1_den->SetLineWidth(2);

  h1_den->Scale(1/(3*max));

  h1_den->Draw("hist same");

  h1_num->SetLineWidth(2);
  h1_num->SetLineColor(kBlack);
  h1_num->SetFillColor(kGray);
  h1_num->SetFillStyle(3254);

  h1_num->SetStats(0);
  h1_num->Scale(1/(3*max));
  
  h1_num->Draw("hist same");


  TLegend *l1 = new TLegend(0.1597744,0.7367742,0.8402256,0.8967742);
  // Making Legends.
  
  //l1->SetNColumns(1);
  l1->SetBorderSize(0);
  l1->SetNColumns(3);
  //l1->SetFillStyle(3245);                                 // Fill the Legend Box.

  l1->SetTextFont(41);
  l1->SetTextAlign(22);
  l1->SetTextSize(0.02);                                  // Setting Text Size, Font, and Align.

  l1->SetFillColor(kWhite);
  l1->SetLineColor(kBlack);
  l1->SetShadowColor(kWhite);                             // Legend Box Styling Options.
  
  l1->AddEntry(h1_den, "Denominator", "l");
  l1->AddEntry(h1_num, "Numerator", "l");                 // Add Entries to the Legend.
  l1->AddEntry(h1_eff, "Efficiency", "lp");                 // Add Entries to the Legend.
  
  l1->Draw("same");
  
  TString outfile = "../Plots/TE/"+year+"/"+var+"_"+hconoff;
  c->Print(outfile+".pdf");                              // Making Output File.
  c->Print(outfile+".C");                                // Making Output File.
  TFile *newfile;
  newfile = new TFile(outfile+".root","recreate");

  h1_eff->Write();                                      // Making Output File.
  newfile->Close();

  return c;
}

