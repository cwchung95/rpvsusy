void run()
{
//=========Macro generated from canvas: c1/c1
//=========  (Thu Dec 26 14:30:53 2019) by ROOT version 6.12/07
   TCanvas *c1 = new TCanvas("c1", "c1",0,23,2400,800);
   c1->SetHighLightColor(2);
   c1->Range(0,0,1,1);
   c1->SetFillColor(0);
   c1->SetBorderMode(0);
   c1->SetBorderSize(2);
   c1->SetFrameBorderMode(0);
  
// ------------>Primitives in pad: c1_1
   TPad *c1_1 = new TPad("c1_1", "c1_1",0.01,0.01,0.3233333,0.99);
   c1_1->Draw();
   c1_1->cd();
   c1_1->Range(-0.375,-163.275,3.375,1469.475);
   c1_1->SetFillColor(0);
   c1_1->SetBorderMode(0);
   c1_1->SetBorderSize(2);
   c1_1->SetFrameBorderMode(0);
   c1_1->SetFrameBorderMode(0);
   
   TH1D *hbkg__1 = new TH1D("hbkg__1","run",3,0,3);
   hbkg__1->SetBinContent(2,1244);
   hbkg__1->SetMinimum(0);
   hbkg__1->SetEntries(1244);
   hbkg__1->SetStats(0);

   Int_t ci;      // for color index setting
   TColor *color; // for color definition with alpha
   ci = TColor::GetColor("#000099");
   hbkg__1->SetLineColor(ci);
   hbkg__1->GetXaxis()->SetRange(1,99);
   hbkg__1->GetXaxis()->SetLabelFont(42);
   hbkg__1->GetXaxis()->SetLabelSize(0.035);
   hbkg__1->GetXaxis()->SetTitleSize(0.035);
   hbkg__1->GetXaxis()->SetTitleFont(42);
   hbkg__1->GetYaxis()->SetLabelFont(42);
   hbkg__1->GetYaxis()->SetLabelSize(0.035);
   hbkg__1->GetYaxis()->SetTitleSize(0.035);
   hbkg__1->GetYaxis()->SetTitleOffset(0);
   hbkg__1->GetYaxis()->SetTitleFont(42);
   hbkg__1->GetZaxis()->SetLabelFont(42);
   hbkg__1->GetZaxis()->SetLabelSize(0.035);
   hbkg__1->GetZaxis()->SetTitleSize(0.035);
   hbkg__1->GetZaxis()->SetTitleFont(42);
   hbkg__1->Draw("");
   
   TPaveText *pt = new TPaveText(0.4494077,0.94,0.5505923,0.995,"blNDC");
   pt->SetName("title");
   pt->SetBorderSize(0);
   pt->SetFillColor(0);
   pt->SetFillStyle(0);
   pt->SetTextFont(42);
   TText *pt_LaTex = pt->AddText("run");
   pt->Draw();
   c1_1->Modified();
   c1->cd();
  
// ------------>Primitives in pad: c1_2
   TPad *c1_2 = new TPad("c1_2", "c1_2",0.3433333,0.01,0.6566667,0.99);
   c1_2->Draw();
   c1_2->cd();
   c1_2->Range(-0.375,-70.48126,3.375,634.3313);
   c1_2->SetFillColor(0);
   c1_2->SetBorderMode(0);
   c1_2->SetBorderSize(2);
   c1_2->SetFrameBorderMode(0);
   c1_2->SetFrameBorderMode(0);
   
   TH1D *hsig__2 = new TH1D("hsig__2","run",3,0,3);
   hsig__2->SetBinContent(2,537);
   hsig__2->SetMinimum(0);
   hsig__2->SetEntries(537);
   hsig__2->SetStats(0);

   ci = TColor::GetColor("#000099");
   hsig__2->SetLineColor(ci);
   hsig__2->GetXaxis()->SetRange(1,99);
   hsig__2->GetXaxis()->SetLabelFont(42);
   hsig__2->GetXaxis()->SetLabelSize(0.035);
   hsig__2->GetXaxis()->SetTitleSize(0.035);
   hsig__2->GetXaxis()->SetTitleFont(42);
   hsig__2->GetYaxis()->SetLabelFont(42);
   hsig__2->GetYaxis()->SetLabelSize(0.035);
   hsig__2->GetYaxis()->SetTitleSize(0.035);
   hsig__2->GetYaxis()->SetTitleOffset(0);
   hsig__2->GetYaxis()->SetTitleFont(42);
   hsig__2->GetZaxis()->SetLabelFont(42);
   hsig__2->GetZaxis()->SetLabelSize(0.035);
   hsig__2->GetZaxis()->SetTitleSize(0.035);
   hsig__2->GetZaxis()->SetTitleFont(42);
   hsig__2->Draw("");
   
   pt = new TPaveText(0.4494077,0.94,0.5505923,0.995,"blNDC");
   pt->SetName("title");
   pt->SetBorderSize(0);
   pt->SetFillColor(0);
   pt->SetFillStyle(0);
   pt->SetTextFont(42);
   pt_LaTex = pt->AddText("run");
   pt->Draw();
   c1_2->Modified();
   c1->cd();
  
// ------------>Primitives in pad: c1_3
   TPad *c1_3 = new TPad("c1_3", "c1_3",0.6766667,0.01,0.99,0.99);
   c1_3->Draw();
   c1_3->cd();
   c1_3->Range(281311,-131.25,284331,1181.25);
   c1_3->SetFillColor(0);
   c1_3->SetBorderMode(0);
   c1_3->SetBorderSize(2);
   c1_3->SetFrameBorderMode(0);
   c1_3->SetFrameBorderMode(0);
   
   TH1D *hdat__3 = new TH1D("hdat__3","run",99,281613,284029);
   hdat__3->SetBinContent(1,27);
   hdat__3->SetBinContent(4,452);
   hdat__3->SetBinContent(5,224);
   hdat__3->SetBinContent(8,721);
   hdat__3->SetBinContent(20,436);
   hdat__3->SetBinContent(46,705);
   hdat__3->SetBinContent(68,387);
   hdat__3->SetBinContent(72,387);
   hdat__3->SetBinContent(94,505);
   hdat__3->SetBinContent(96,1000);
   hdat__3->SetBinContent(100,237);
   hdat__3->SetMinimum(0);
   hdat__3->SetEntries(5081);
   hdat__3->SetStats(0);

   ci = TColor::GetColor("#000099");
   hdat__3->SetLineColor(ci);
   hdat__3->GetXaxis()->SetLabelFont(42);
   hdat__3->GetXaxis()->SetLabelSize(0.035);
   hdat__3->GetXaxis()->SetTitleSize(0.035);
   hdat__3->GetXaxis()->SetTitleFont(42);
   hdat__3->GetYaxis()->SetLabelFont(42);
   hdat__3->GetYaxis()->SetLabelSize(0.035);
   hdat__3->GetYaxis()->SetTitleSize(0.035);
   hdat__3->GetYaxis()->SetTitleOffset(0);
   hdat__3->GetYaxis()->SetTitleFont(42);
   hdat__3->GetZaxis()->SetLabelFont(42);
   hdat__3->GetZaxis()->SetLabelSize(0.035);
   hdat__3->GetZaxis()->SetTitleSize(0.035);
   hdat__3->GetZaxis()->SetTitleFont(42);
   hdat__3->Draw("");
   
   pt = new TPaveText(0.4494077,0.94,0.5505923,0.995,"blNDC");
   pt->SetName("title");
   pt->SetBorderSize(0);
   pt->SetFillColor(0);
   pt->SetFillStyle(0);
   pt->SetTextFont(42);
   pt_LaTex = pt->AddText("run");
   pt->Draw();
   c1_3->Modified();
   c1->cd();
   c1->Modified();
   c1->cd();
   c1->SetSelected(c1);
}
