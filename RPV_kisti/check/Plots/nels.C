void nels()
{
//=========Macro generated from canvas: c1/c1
//=========  (Thu Dec 26 14:37:06 2019) by ROOT version 6.12/07
   TCanvas *c1 = new TCanvas("c1", "c1",0,23,2400,800);
   c1->SetHighLightColor(2);
   c1->Range(0,0,1,1);
   c1->SetFillColor(0);
   c1->SetBorderMode(0);
   c1->SetBorderSize(2);
   c1->SetFrameBorderMode(0);
  
// ------------>Primitives in pad: c1_1
   TPad *c1_1 = new TPad("c1_1", "c1_1",0.01,0.01,0.3233333,0.99);
   c1_1->Draw();
   c1_1->cd();
   c1_1->Range(-0.25,-148.3125,2.25,1334.813);
   c1_1->SetFillColor(0);
   c1_1->SetBorderMode(0);
   c1_1->SetBorderSize(2);
   c1_1->SetFrameBorderMode(0);
   c1_1->SetFrameBorderMode(0);
   
   TH1D *hbkg__67 = new TH1D("hbkg__67","nels",2,0,2);
   hbkg__67->SetBinContent(1,1130);
   hbkg__67->SetBinContent(2,112);
   hbkg__67->SetBinContent(3,2);
   hbkg__67->SetMinimum(0);
   hbkg__67->SetEntries(1244);
   hbkg__67->SetStats(0);

   Int_t ci;      // for color index setting
   TColor *color; // for color definition with alpha
   ci = TColor::GetColor("#000099");
   hbkg__67->SetLineColor(ci);
   hbkg__67->GetXaxis()->SetLabelFont(42);
   hbkg__67->GetXaxis()->SetLabelSize(0.035);
   hbkg__67->GetXaxis()->SetTitleSize(0.035);
   hbkg__67->GetXaxis()->SetTitleFont(42);
   hbkg__67->GetYaxis()->SetLabelFont(42);
   hbkg__67->GetYaxis()->SetLabelSize(0.035);
   hbkg__67->GetYaxis()->SetTitleSize(0.035);
   hbkg__67->GetYaxis()->SetTitleOffset(0);
   hbkg__67->GetYaxis()->SetTitleFont(42);
   hbkg__67->GetZaxis()->SetLabelFont(42);
   hbkg__67->GetZaxis()->SetLabelSize(0.035);
   hbkg__67->GetZaxis()->SetTitleSize(0.035);
   hbkg__67->GetZaxis()->SetTitleFont(42);
   hbkg__67->Draw("");
   
   TPaveText *pt = new TPaveText(0.4387605,0.94,0.5612395,0.995,"blNDC");
   pt->SetName("title");
   pt->SetBorderSize(0);
   pt->SetFillColor(0);
   pt->SetFillStyle(0);
   pt->SetTextFont(42);
   TText *pt_LaTex = pt->AddText("nels");
   pt->Draw();
   c1_1->Modified();
   c1->cd();
  
// ------------>Primitives in pad: c1_2
   TPad *c1_2 = new TPad("c1_2", "c1_2",0.3433333,0.01,0.6566667,0.99);
   c1_2->Draw();
   c1_2->cd();
   c1_2->Range(-0.25,-61.03125,2.25,549.2813);
   c1_2->SetFillColor(0);
   c1_2->SetBorderMode(0);
   c1_2->SetBorderSize(2);
   c1_2->SetFrameBorderMode(0);
   c1_2->SetFrameBorderMode(0);
   
   TH1D *hsig__68 = new TH1D("hsig__68","nels",2,0,2);
   hsig__68->SetBinContent(1,465);
   hsig__68->SetBinContent(2,70);
   hsig__68->SetBinContent(3,2);
   hsig__68->SetMinimum(0);
   hsig__68->SetEntries(537);
   hsig__68->SetStats(0);

   ci = TColor::GetColor("#000099");
   hsig__68->SetLineColor(ci);
   hsig__68->GetXaxis()->SetLabelFont(42);
   hsig__68->GetXaxis()->SetLabelSize(0.035);
   hsig__68->GetXaxis()->SetTitleSize(0.035);
   hsig__68->GetXaxis()->SetTitleFont(42);
   hsig__68->GetYaxis()->SetLabelFont(42);
   hsig__68->GetYaxis()->SetLabelSize(0.035);
   hsig__68->GetYaxis()->SetTitleSize(0.035);
   hsig__68->GetYaxis()->SetTitleOffset(0);
   hsig__68->GetYaxis()->SetTitleFont(42);
   hsig__68->GetZaxis()->SetLabelFont(42);
   hsig__68->GetZaxis()->SetLabelSize(0.035);
   hsig__68->GetZaxis()->SetTitleSize(0.035);
   hsig__68->GetZaxis()->SetTitleFont(42);
   hsig__68->Draw("");
   
   pt = new TPaveText(0.4387605,0.94,0.5612395,0.995,"blNDC");
   pt->SetName("title");
   pt->SetBorderSize(0);
   pt->SetFillColor(0);
   pt->SetFillStyle(0);
   pt->SetTextFont(42);
   pt_LaTex = pt->AddText("nels");
   pt->Draw();
   c1_2->Modified();
   c1->cd();
  
// ------------>Primitives in pad: c1_3
   TPad *c1_3 = new TPad("c1_3", "c1_3",0.6766667,0.01,0.99,0.99);
   c1_3->Draw();
   c1_3->cd();
   c1_3->Range(-0.25,-663.6,2.25,5972.4);
   c1_3->SetFillColor(0);
   c1_3->SetBorderMode(0);
   c1_3->SetBorderSize(2);
   c1_3->SetFrameBorderMode(0);
   c1_3->SetFrameBorderMode(0);
   
   TH1D *hdat__69 = new TH1D("hdat__69","nels",2,0,2);
   hdat__69->SetBinContent(1,5056);
   hdat__69->SetBinContent(2,24);
   hdat__69->SetBinContent(3,1);
   hdat__69->SetMinimum(0);
   hdat__69->SetEntries(5081);
   hdat__69->SetStats(0);

   ci = TColor::GetColor("#000099");
   hdat__69->SetLineColor(ci);
   hdat__69->GetXaxis()->SetLabelFont(42);
   hdat__69->GetXaxis()->SetLabelSize(0.035);
   hdat__69->GetXaxis()->SetTitleSize(0.035);
   hdat__69->GetXaxis()->SetTitleFont(42);
   hdat__69->GetYaxis()->SetLabelFont(42);
   hdat__69->GetYaxis()->SetLabelSize(0.035);
   hdat__69->GetYaxis()->SetTitleSize(0.035);
   hdat__69->GetYaxis()->SetTitleOffset(0);
   hdat__69->GetYaxis()->SetTitleFont(42);
   hdat__69->GetZaxis()->SetLabelFont(42);
   hdat__69->GetZaxis()->SetLabelSize(0.035);
   hdat__69->GetZaxis()->SetTitleSize(0.035);
   hdat__69->GetZaxis()->SetTitleFont(42);
   hdat__69->Draw("");
   
   pt = new TPaveText(0.4387605,0.94,0.5612395,0.995,"blNDC");
   pt->SetName("title");
   pt->SetBorderSize(0);
   pt->SetFillColor(0);
   pt->SetFillStyle(0);
   pt->SetTextFont(42);
   pt_LaTex = pt->AddText("nels");
   pt->Draw();
   c1_3->Modified();
   c1->cd();
   c1->Modified();
   c1->cd();
   c1->SetSelected(c1);
}
